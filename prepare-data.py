import logging
import logging
import typer
import requests
import datetime
from pathlib import Path
from crutils import all_members
import pandas as pd
from functools import lru_cache


# Columns we want stored as ints
COUNT_COLUMNS = ["total-dois", "current-dois", "backfile-dois"]
YEAR_COLUMNS = ["earliest-publication-year", "latest-publication-year"]
FLOAT_COLUMNS = [
    "annual-fee",
    "affiliations-current",
    "similarity-checking-current",
    "descriptions-current",
    "ror-ids-current",
    "funders-backfile",
    "licenses-backfile",
    "funders-current",
    "affiliations-backfile",
    "resource-links-backfile",
    "orcids-backfile",
    "update-policies-current",
    "ror-ids-backfile",
    "orcids-current",
    "similarity-checking-backfile",
    "references-backfile",
    "descriptions-backfile",
    "award-numbers-backfile",
    "update-policies-backfile",
    "licenses-current",
    "award-numbers-current",
    "abstracts-backfile",
    "resource-links-current",
    "abstracts-current",
    "references-current",
    "overall-coverage",
    "overall-impact",
]

BOARD_MEMBERS = pd.read_csv("board_ids.csv")
CURRENT_BOARD_IDS = BOARD_MEMBERS.loc[BOARD_MEMBERS["status"] == "current"][
    "id"
].tolist()
PAST_BOARD_IDS = BOARD_MEMBERS.loc[BOARD_MEMBERS["status"] == "past"][
    "id"
].tolist()


def board_status(member_id):
    if member_id in CURRENT_BOARD_IDS:
        return "current"
    elif member_id in PAST_BOARD_IDS:
        return "past"
    else:
        return "none"


@lru_cache()
def crossref_doi_count():
    logging.info("Getting Crossref DOI count")
    return requests.get("https://api.crossref.org/works?rows=0").json()[
        "message"
    ]["total-results"]


def sense_check(member):
    try:
        return (
            member["counts"]["total-dois"] > 0
            and member["cr-labs-member-profile"]["account-type"]
            != "Uncategorized"
        )
    except KeyError as e:
        # logging.error(f"Error checking sanity of {member['id']}: {e}\n {member}")
        logging.error(f"Error checking sanity of {member['id']}")

        return False


def calculate_active_years(member: dict) -> tuple:
    """extract all the years from breakdowns, return tuple of first & last years"""
    publishing_years = [
        entry[0]
        for entry in member["breakdowns"]["dois-by-issued-year"]
        if entry[1] > 0
    ] or [datetime.datetime.now().year + 1]
    return (min(publishing_years), max(publishing_years))


def annual_breakdowns(member, start_year=2010, end_year=2022):
    years = list(range(start_year, end_year))
    history = dict(member["breakdowns"]["dois-by-issued-year"])
    year_totals = {f"{year}-total": history.get(year, None) for year in years}

    # Eventualy create columns showing change from previous year?
    # pct_change = pd.Series(year_totals.values()).pct_change().fillna(0).tolist()
    # year_changes = {f"{year}-pct-change":pct_change[i] for i, year in enumerate(years) if i > 0}

    avg_pct_change = pd.Series(year_totals.values()).pct_change().mean()

    # If there is only one year of data, the pct_change will be NaN
    # This is because there is no previous year to compare to
    # So we set it to 0. This is likely to be some sort of statistical
    # idiocy.

    avg_pct_change = 0 if pd.isna(avg_pct_change) else avg_pct_change

    return year_totals | {"avg-pct-change": avg_pct_change}


def summarize(member):
    logging.debug(f"Summarizing {member['id']}")
    try:
        summary = {
            "id": member["id"],
            "primary-name": member["primary-name"],
            "account-type": member["cr-labs-member-profile"]["account-type"],
            "non-profit": member["cr-labs-member-profile"]["non-profit"],
            "date-joined": member["cr-labs-member-profile"]["date-joined"],
            "annual-fee": member["cr-labs-member-profile"]["annual-fee"],
            "active": member["cr-labs-member-profile"]["inactive"] == "No",
            "board-status": board_status(member["id"]),
            "affiliations-current": member["coverage"]["affiliations-current"],
            "similarity-checking-current": member["coverage"][
                "similarity-checking-current"
            ],
            "descriptions-current": member["coverage"]["descriptions-current"],
            "ror-ids-current": member["coverage"]["ror-ids-current"],
            "funders-backfile": member["coverage"]["funders-backfile"],
            "licenses-backfile": member["coverage"]["licenses-backfile"],
            "funders-current": member["coverage"]["funders-current"],
            "affiliations-backfile": member["coverage"][
                "affiliations-backfile"
            ],
            "resource-links-backfile": member["coverage"][
                "resource-links-backfile"
            ],
            "orcids-backfile": member["coverage"]["orcids-backfile"],
            "update-policies-current": member["coverage"][
                "update-policies-current"
            ],
            "ror-ids-backfile": member["coverage"]["ror-ids-backfile"],
            "orcids-current": member["coverage"]["orcids-current"],
            "similarity-checking-backfile": member["coverage"][
                "similarity-checking-backfile"
            ],
            "references-backfile": member["coverage"]["references-backfile"],
            "descriptions-backfile": member["coverage"][
                "descriptions-backfile"
            ],
            "award-numbers-backfile": member["coverage"][
                "award-numbers-backfile"
            ],
            "update-policies-backfile": member["coverage"][
                "update-policies-backfile"
            ],
            "licenses-current": member["coverage"]["licenses-current"],
            "award-numbers-current": member["coverage"][
                "award-numbers-current"
            ],
            "abstracts-backfile": member["coverage"]["abstracts-backfile"],
            "resource-links-current": member["coverage"][
                "resource-links-current"
            ],
            "abstracts-current": member["coverage"]["abstracts-current"],
            "references-current": member["coverage"]["references-current"],
        }
        summary |= member["counts"]
        summary |= member["coverage"]
        overall_coverage = pd.Series(member["coverage"].values()).mean()
        summary |= {"overall-coverage": overall_coverage}
        overall_impact = (
            overall_coverage * summary["total-dois"]
        ) / crossref_doi_count()
        summary |= {"overall-impact": overall_impact}
        (
            earliest_publication_year,
            latest_publication_year,
        ) = calculate_active_years(member)
        summary |= {
            "earliest-publication-year": earliest_publication_year,
            "latest-publication-year": latest_publication_year,
        }
        summary |= annual_breakdowns(member)
    except Exception as e:
        logging.error(f"Error summarizing {member['id']}: {e}")
        raise e
    return summary


def convert_to_int(df, columns):
    for column in columns:
        if column in df:
            df[column] = df[column].astype("Int64")
    return df


def convert_to_float(df, columns):
    for column in columns:
        if column in df:
            df[column] = df[column].astype("float64")
    return df


def cleanup_dataframe(df):
    # fill missing values with zero
    df = df.fillna(0)
    df["annual-fee"] = df["annual-fee"].replace("", 0)

    df = convert_to_int(df, COUNT_COLUMNS + YEAR_COLUMNS + ["id"])
    df = convert_to_float(df, FLOAT_COLUMNS)
    # if account-type is nul, replace with "Uncategorized"
    df["account-type"] = df["account-type"].replace("", "Uncategorized")
    return df


def main(
    verbose: bool = typer.Option(False, "--verbose", "-v"),
):
    from smart_open import open

    with open(
        "s3://outputs.research.crossref.org/api_artifacts/board_ids.csv", "w"
    ) as f:
        with open("board_ids.csv", "r") as g:
            f.write(g.read())

    return
    """A simple CLI for testing typer"""
    if verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    typer.echo(f"Verbose: {verbose}")

    try:
        summaries = {
            member["id"]: summarize(member)
            for member in all_members()
            if sense_check(member)
        }
        df = pd.DataFrame(summaries).transpose()
        df = cleanup_dataframe(df)
        df.to_parquet("members.parquet")
        logging.info(f"Summarized {len(summaries)} members")
    except Exception as e:
        typer.echo(f"Error: {e}")
        raise typer.Exit(code=1) from e


if __name__ == "__main__":
    typer.run(main)
