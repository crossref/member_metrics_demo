import logging

import pendulum
from airflow.decorators import dag, task

DAG_ID = "member-metrics"

DEFAULT_ARGS = {
    "owner": "meve",
    "depends_on_past": False,
    "email": ["labs@crossref.org"],
    "email_on_failure": False,
    "email_on_retry": False,
}

logger = logging.getLogger(__name__)


@dag(
    dag_id=f"{DAG_ID}",
    default_args=DEFAULT_ARGS,
    schedule="@daily",
    start_date=pendulum.datetime(2024, 7, 31, tz="UTC"),
    catchup=True,
    tags=["labs", "metrics"],
    owner_links={"mpeve": "https://www.crossref.org/people/martin-eve/"},
    description="update the member metrics data",
)
def update_member_metrics():
    @task.virtualenv(
        requirements=[
            "boto3",
            "joblib",
            "pendulum",
            "claws==0.0.21",
            "ijson",
            "requests",
            "smart_open",
            "pendulum",
        ],
        system_site_packages=True,
    )
    def prepare_data():
        from functools import lru_cache
        import pandas as pd

        def board_status(member_id):
            if member_id in current_board_ids:
                return "current"
            elif member_id in past_board_ids:
                return "past"
            else:
                return "none"

        @lru_cache()
        def crossref_doi_count():
            import requests
            import logging

            logging.info("Getting Crossref DOI count")
            return requests.get("https://api.crossref.org/works?rows=0").json()[
                "message"
            ]["total-results"]

        def sense_check(member):
            try:
                return (
                    member["counts"]["total-dois"] > 0
                    and member["cr-labs-member-profile"]["account-type"]
                    != "Uncategorized"
                )
            except KeyError:
                import logging

                logging.error(f"Error checking sanity of {member['id']}")

                return False

        def calculate_active_years(member: dict) -> tuple:
            import datetime

            # extract all the years from breakdowns, return tuple of first &
            # last years
            publishing_years = [
                entry[0]
                for entry in member["breakdowns"]["dois-by-issued-year"]
                if entry[1] > 0
            ] or [datetime.datetime.now().year + 1]
            return min(publishing_years), max(publishing_years)

        def annual_breakdowns(member, start_year=2010, end_year=2022):
            years = list(range(start_year, end_year))
            history = dict(member["breakdowns"]["dois-by-issued-year"])
            year_totals = {
                f"{year}-total": history.get(year, None) for year in years
            }

            avg_pct_change = pd.Series(year_totals.values()).pct_change().mean()

            # If there is only one year of data, the pct_change will be NaN
            # This is because there is no previous year to compare to
            # So we set it to 0. This is likely to be some sort of statistical
            # idiocy.

            avg_pct_change = 0 if pd.isna(avg_pct_change) else avg_pct_change

            return year_totals | {"avg-pct-change": avg_pct_change}

        def summarize(member):
            import logging

            logging.debug(f"Summarizing {member['id']}")
            try:
                summary = {
                    "id": member["id"],
                    "primary-name": member["primary-name"],
                    "account-type": member["cr-labs-member-profile"][
                        "account-type"
                    ],
                    "non-profit": member["cr-labs-member-profile"][
                        "non-profit"
                    ],
                    "date-joined": member["cr-labs-member-profile"][
                        "date-joined"
                    ],
                    "annual-fee": member["cr-labs-member-profile"][
                        "annual-fee"
                    ],
                    "active": member["cr-labs-member-profile"]["inactive"]
                    == "No",
                    "board-status": board_status(member["id"]),
                    "affiliations-current": member["coverage"][
                        "affiliations-current"
                    ],
                    "similarity-checking-current": member["coverage"][
                        "similarity-checking-current"
                    ],
                    "descriptions-current": member["coverage"][
                        "descriptions-current"
                    ],
                    "ror-ids-current": member["coverage"]["ror-ids-current"],
                    "funders-backfile": member["coverage"]["funders-backfile"],
                    "licenses-backfile": member["coverage"][
                        "licenses-backfile"
                    ],
                    "funders-current": member["coverage"]["funders-current"],
                    "affiliations-backfile": member["coverage"][
                        "affiliations-backfile"
                    ],
                    "resource-links-backfile": member["coverage"][
                        "resource-links-backfile"
                    ],
                    "orcids-backfile": member["coverage"]["orcids-backfile"],
                    "update-policies-current": member["coverage"][
                        "update-policies-current"
                    ],
                    "ror-ids-backfile": member["coverage"]["ror-ids-backfile"],
                    "orcids-current": member["coverage"]["orcids-current"],
                    "similarity-checking-backfile": member["coverage"][
                        "similarity-checking-backfile"
                    ],
                    "references-backfile": member["coverage"][
                        "references-backfile"
                    ],
                    "descriptions-backfile": member["coverage"][
                        "descriptions-backfile"
                    ],
                    "award-numbers-backfile": member["coverage"][
                        "award-numbers-backfile"
                    ],
                    "update-policies-backfile": member["coverage"][
                        "update-policies-backfile"
                    ],
                    "licenses-current": member["coverage"]["licenses-current"],
                    "award-numbers-current": member["coverage"][
                        "award-numbers-current"
                    ],
                    "abstracts-backfile": member["coverage"][
                        "abstracts-backfile"
                    ],
                    "resource-links-current": member["coverage"][
                        "resource-links-current"
                    ],
                    "abstracts-current": member["coverage"][
                        "abstracts-current"
                    ],
                    "references-current": member["coverage"][
                        "references-current"
                    ],
                }
                summary |= member["counts"]
                summary |= member["coverage"]
                overall_coverage = pd.Series(member["coverage"].values()).mean()
                summary |= {"overall-coverage": overall_coverage}
                overall_impact = (
                    overall_coverage * summary["total-dois"]
                ) / crossref_doi_count()
                summary |= {"overall-impact": overall_impact}
                (
                    earliest_publication_year,
                    latest_publication_year,
                ) = calculate_active_years(member)
                summary |= {
                    "earliest-publication-year": earliest_publication_year,
                    "latest-publication-year": latest_publication_year,
                }
                summary |= annual_breakdowns(member)
            except Exception as e:
                logging.error(f"Error summarizing {member['id']}: {e}")
                raise e
            return summary

        def convert_to_int(data_frame, columns):
            for column in columns:
                if column in data_frame:
                    data_frame[column] = data_frame[column].astype("Int64")
            return data_frame

        def convert_to_float(data_frame, columns):
            for column in columns:
                if column in data_frame:
                    data_frame[column] = data_frame[column].astype("float64")
            return data_frame

        def cleanup_dataframe(data_frame):
            # fill missing values with zero
            data_frame = data_frame.fillna(0)
            data_frame["annual-fee"] = data_frame["annual-fee"].replace("", 0)

            data_frame = convert_to_int(
                data_frame, count_columns + year_columns + ["id"]
            )
            data_frame = convert_to_float(data_frame, float_columns)
            # if account-type is null, replace with "Uncategorized"
            data_frame["account-type"] = data_frame["account-type"].replace(
                "", "Uncategorized"
            )
            return data_frame

        def all_members():
            # download the members.json file from S3
            members_file = (
                "s3://outputs.research.crossref.org/api_artifacts/members.json"
            )

            from smart_open import open
            import ijson

            with open(members_file, "rb") as f:
                yield from ijson.items(f, "message.items.item")

        # main task flow

        # Columns we want stored as ints
        count_columns = ["total-dois", "current-dois", "backfile-dois"]
        year_columns = ["earliest-publication-year", "latest-publication-year"]
        float_columns = [
            "annual-fee",
            "affiliations-current",
            "similarity-checking-current",
            "descriptions-current",
            "ror-ids-current",
            "funders-backfile",
            "licenses-backfile",
            "funders-current",
            "affiliations-backfile",
            "resource-links-backfile",
            "orcids-backfile",
            "update-policies-current",
            "ror-ids-backfile",
            "orcids-current",
            "similarity-checking-backfile",
            "references-backfile",
            "descriptions-backfile",
            "award-numbers-backfile",
            "update-policies-backfile",
            "licenses-current",
            "award-numbers-current",
            "abstracts-backfile",
            "resource-links-current",
            "abstracts-current",
            "references-current",
            "overall-coverage",
            "overall-impact",
        ]

        from smart_open import open

        with open(
            "s3://outputs.research.crossref.org/api_artifacts/board_ids.csv",
            "rb",
        ) as f:
            board_members = pd.read_csv(f)

        current_board_ids = board_members.loc[
            board_members["status"] == "current"
        ]["id"].tolist()
        past_board_ids = board_members.loc[board_members["status"] == "past"][
            "id"
        ].tolist()

        summaries = {
            member["id"]: summarize(member)
            for member in all_members()
            if sense_check(member)
        }
        df = pd.DataFrame(summaries).transpose()
        df = cleanup_dataframe(df)

        from smart_open import open

        with open(
            "s3://outputs.research.crossref.org/"
            "member-metrics/members.parquet",
            "wb",
        ) as fh:
            df.to_parquet(fh)

        import pendulum

        with open(
            "s3://outputs.research.crossref.org/"
            "member-metrics/created_at.txt",
            "wb",
        ) as fh:
            fh.write(str(pendulum.now().timestamp()).encode())

        import logging

        logging.info(f"Summarized {len(summaries)} members")

    prepare_data()


update_member_metrics()
