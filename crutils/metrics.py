import datetime
import logging
import gc
import pandas as pd
import requests
import ijson

import streamlit as st

#from crapi import all_members  

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


TOP = 10

BOARD_MEMBERS = pd.read_csv("board_ids.csv")
CURRENT_BOARD_IDS = BOARD_MEMBERS.loc[BOARD_MEMBERS["status"] == "current"]["id"].tolist()
PAST_BOARD_IDS = BOARD_MEMBERS.loc[BOARD_MEMBERS["status"] == "past"]["id"].tolist()

SYNTHETIC_SUFFIX = "impact"
SYNTHETIC_COLUMNS = ["overall-coverage", f"overall-{SYNTHETIC_SUFFIX}"]


COUNT_COLUMNS = ["total-dois", "current-dois", "backfile-dois"]
YEAR_COLUMNS = ["earliest-publication-year", "latest-publication-year"]

COVERAGE_PERCENT_COLUMNS = [
    "affiliations-current",
    "similarity-checking-current",
    "funders-backfile",
    "licenses-backfile",
    "funders-current",
    "affiliations-backfile",
    "resource-links-backfile",
    "orcids-backfile",
    "update-policies-current",
    "open-references-backfile",
    "orcids-current",
    "similarity-checking-backfile",
    "references-backfile",
    "award-numbers-backfile",
    "update-policies-backfile",
    "licenses-current",
    "award-numbers-current",
    "abstracts-backfile",
    "resource-links-current",
    "abstracts-current",
    "open-references-current",
    "references-current",
]
COVERAGE_TOTAL_COLUMNS = [f"{name}-total" for name in COVERAGE_PERCENT_COLUMNS]
COVERAGE_COLUMNS = sorted(COVERAGE_PERCENT_COLUMNS + COVERAGE_TOTAL_COLUMNS)
BASE_COLUMNS = ["primary-name", "id", "account-type", "non-profit", "date-joined", "annual-fee", "board-member", "active"] + SYNTHETIC_COLUMNS
CHECK_COLUMN = ["record-check"]

COLUMN_ORDER = (
    BASE_COLUMNS + YEAR_COLUMNS + COUNT_COLUMNS + COVERAGE_COLUMNS + CHECK_COLUMN
)

DISPLAY_FORMATS = {
    "id": st.column_config.NumberColumn(
        help="Crossref member ID",
        format="%d",
    ),
    "primary-name": st.column_config.TextColumn(
        help="Crossref member name",
    ),
    "earliest-publication-year": st.column_config.NumberColumn(
        help="earliest publication year of member's content",
        format="%d",
    ),
    "latest-publication-year": st.column_config.NumberColumn(
        help="latest publication year of member's content",
        format="%d",
    ),
    # "annual-fee": st.column_config.NumberColumn(
    #     help="Crossref annual membership fee",
    #     format="$ %.2f",
    # ),
}

DISPLAY_COLUMNS = [
    "id",
    "primary-name",
    "account-type",
    "non-profit",
    "date-joined",
    "annual-fee",
    "active",
    "board-member",
    "overall-coverage",
    f"overall-{SYNTHETIC_SUFFIX}",
    "earliest-publication-year",
    "latest-publication-year",
    "total-dois",
    "current-dois",
    "backfile-dois",
]

summary_df = None


#TOTAL_CROSSREF_DOIS = total_dois()

def total_dois():
    return requests.get("https://api.crossref.org/works?rows=0").json()["message"]["total-results"]


def get_board_members():
    return pd.read_csv("board_ids.csv")


def convert_to_int(df, columns):
    for column in columns:
        if column in df:
            df[column] = df[column].astype("Int64")
    return df


def calculate_active_years(member: dict) -> tuple:
    """extract all the years from breakdowns, return tuple of first & last years"""
    publishing_years = [
        entry[0]
        for entry in member["breakdowns"]["dois-by-issued-year"]
        if entry[1] > 0
    ]
    # If we see no activity, set activity to the future (next year)
    if len(publishing_years) == 0:
        publishing_years = [datetime.datetime.today().year + 1]
    return (min(publishing_years), max(publishing_years))


def board_status(member_id):
    if member_id in CURRENT_BOARD_IDS:
        return "current"
    elif member_id in PAST_BOARD_IDS:
        return "past"
    else:
        return "no"

def summarize(member: dict) -> dict:
    # API returns some broken member records
    # we need to flag those that have null counts
    sane = bool(member["counts"])


    # The minimal data
    summary = {
        "id": member["id"],
        "record-check": sane,
        "primary-name": member["primary-name"],
        "account-type": member["cr-labs-member-profile"]["account-type"],
        "non-profit": member["cr-labs-member-profile"]["non-profit"],
        "date-joined": member["cr-labs-member-profile"]["date-joined"],
        "annual-fee": member["cr-labs-member-profile"]["annual-fee"],
        "active": True if member["cr-labs-member-profile"]["inactive"] == "No" else False,
        "board-member": board_status(member["id"]),
    }

    if sane:
        # Get the rest of the data
        summary |= member["coverage"]
        summary |= member["counts"]
        earliest_publication_year, latest_publication_year = calculate_active_years(
            member
        )
        summary |= {
            "earliest-publication-year": earliest_publication_year,
            "latest-publication-year": latest_publication_year,
        }

    logger.debug(f"sumary:{summary}")
    return summary


def cleanup_dataframe(df):
    # fill missing values with zero
    df = df.fillna(0)
    df = convert_to_int(df, COUNT_COLUMNS + YEAR_COLUMNS)
    # replace annual-fee with 0 if is is a string
    df["annual-fee"] = df["annual-fee"].replace("", 0)
    df = df.astype({"annual-fee": "float64"})
    # if account-type is nul, replace with "Uncategorized"
    df["account-type"] = df["account-type"].replace("", "Uncategorized")
    return df


def add_overall_coverage(df):
    coverage_df = df[COVERAGE_PERCENT_COLUMNS]
    # convert all columns to float
    coverage_df = coverage_df.astype("float64")

    df["overall-coverage"] = coverage_df.mean(axis=1)
    # df[f"overall-{SYNTHETIC_SUFFIX}"] = round(
    #     (df["overall-coverage"] * df["total-dois"]) / TOTAL_CROSSREF_DOIS, 4
    # ) TODO
    df[f"overall-{SYNTHETIC_SUFFIX}"] = (
        df["overall-coverage"] * df["total-dois"]
    ) / total_dois() # TOTAL_CROSSREF_DOIS

    return df


def add_estimated_totals(df):
    for column_name in COVERAGE_PERCENT_COLUMNS:
        metric_name, period = column_name.rsplit("-", 1)
        period_total = f"{period}-dois"
        subtotal_name = f"{column_name}-total"
        # df[subtotal_name] = df[column_name].round(2) * df[period_total] TODO
        df[subtotal_name] = df[column_name] * df[period_total]

        df[subtotal_name] = df[subtotal_name].astype("int64")
    return df


def format_as_percentage(df, c):
    df["overall-coverage"] = pd.Series(
        ["{0:.2f}%".format(val) for val in df["overall-coverage"]], index=df.index
    )
    df["overall-impact"] = pd.Series(
        ["{0:.2f}%".format(val) for val in df["overall-impact"]], index=df.index
    )
    # TODO fix so not hard-coded. But code below is giving me angina.
    # top_by_coverage['overall-impact'] = pd.Series(["{0:.2f}%".format(val * 100) for val in top_by_coverage['overall-impact']], index = top_by_coverage.index)
    # for column in columns:
    #     df[column] = pd.Series(
    #         ["{0:.2f}%".format(val) for val in df[column]], index=df.index
    #     )

    return df

def all_members():
    with open("members.json", "rb") as f:
        yield from ijson.items(f, "message.items.item")

def get_metrics():
    #members = all_members()

    logger.info("summarizing members")
    summary = [summarize(member_record) for member_record in all_members()]
    logger.info("finished summarizing members")
    logger.info("creating dataframe")
    df = pd.DataFrame(summary)
    # filter dataframe to show only members whos account-type is "Member Publisher" or "Sponsored Publisher"
    logger.info("filtering dataframe")
    #df = df[df["account-type"].isin(["Member Publisher", "Sponsored Publisher", ""Sponsoring Publisher", "REPRESENTED_MEMBER"])]
    # filter dataframe to show only members whos total-dois is greater than 0
    df = df[df["total-dois"] > 0]
    logger.info("reindexing dataframe")
    df = df.reindex(COLUMN_ORDER, axis=1)
    logger.info("cleaning dataframe")
    df = cleanup_dataframe(df)
    logger.info("adding overall coverage")
    df = add_overall_coverage(df)
    logger.info("adding estimated totals")
    df = add_estimated_totals(df)
    # df = format_as_percentage(df, ["overall-coverage", "overall-impact"])

    return df
