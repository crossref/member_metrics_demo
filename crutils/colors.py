"""Python colormaps  """


import matplotlib
import matplotlib.colors as col
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np

CR_PRIMARY_RED = "#ef3340"
CR_PRIMARY_GREEN = "#3eb1c8"
CR_PRIMARY_YELLOW = "#ffc72c"
CR_PRIMARY_LT_GREY = "#d8d2c4"
CR_PRIMARY_DK_GREY = "#4f5858"

CR_SECONDARY_RED = "#a6192e"
CR_SECONDARY_GREEN = "#00ab84"
CR_SECONDARY_YELLOW = "#ffa300"
CR_SECONDARY_BLUE = "#005f83"
CR_SECONDARY_LT_GREY = "#a39382"

CR_MONO_1 = "#d9d9d9"
CR_MONO_2 = "#bfbfbf"
CR_MONO_3 = "#a6a6a6"
CR_MONO_4 = "#595959"
CR_MONO_5 = "#333333"


def make_gradient_cmap(name, startcolor, midcolor, endcolor):
    cmap = col.LinearSegmentedColormap.from_list(name, [startcolor, midcolor, endcolor])
    cm.register_cmap(cmap=cmap)
    return name


def make_discrete_cmap(name, cpool, N=8):
    cmap = col.ListedColormap(cpool[0:N], name)
    cm.register_cmap(cmap=cmap)
    return name


def show_cmaps(names=None):
    """display all colormaps included in the names list. If names is None, all
    defined colormaps will be shown."""
    # base code from http://www.scipy.org/Cookbook/Matplotlib/Show_colormaps
    matplotlib.rc("text", usetex=False)
    a = np.outer(np.arange(0, 1, 0.01), np.ones(10))  # pseudo image data
    f = plt.figure(figsize=(10, 5))
    f.subplots_adjust(top=0.8, bottom=0.05, left=0.01, right=0.99)
    # get list of all colormap names
    # this only obtains names of built-in colormaps:
    maps = [m for m in cm.datad if not m.endswith("_r")]
    # use undocumented cmap_d dictionary instead
    maps = [m for m in cm.cmap_d if not m.endswith("_r")]
    maps.sort()
    # determine number of subplots to make
    l = len(maps) + 1
    if names is not None:
        l = len(names)  # assume all names are correct!
    # loop over maps and plot the selected ones
    i = 0
    for m in maps:
        if names is None or m in names:
            i += 1
            ax = plt.subplot(1, l, i)
            ax.axis("off")
            plt.imshow(a, aspect="auto", cmap=cm.get_cmap(m), origin="lower")
            plt.title(m, rotation=90, fontsize=10, verticalalignment="bottom")
    plt.savefig("crossref_colormaps.png", dpi=100, facecolor="gray")


CR_PRIMARY_COLORMAP = make_discrete_cmap(
    "crossref-primary",
    [
        CR_PRIMARY_RED,
        CR_PRIMARY_GREEN,
        CR_PRIMARY_YELLOW,
        CR_PRIMARY_LT_GREY,
        CR_PRIMARY_DK_GREY,
    ],
)
CR_SECONDARY_COLORMAP = make_discrete_cmap(
    "crossref-secondary",
    [
        CR_SECONDARY_RED,
        CR_SECONDARY_GREEN,
        CR_SECONDARY_YELLOW,
        CR_SECONDARY_BLUE,
        CR_SECONDARY_LT_GREY,
    ],
)
CR_MONO_COLORMAP = make_discrete_cmap(
    "crossref-mono", [CR_MONO_1, CR_MONO_2, CR_MONO_3, CR_MONO_4, CR_MONO_5]
)

CR_PRIMARY_GRADIENT = make_gradient_cmap(
    "crossref-gradient-primary", CR_PRIMARY_GREEN, CR_PRIMARY_YELLOW, CR_PRIMARY_RED
)
CR_SECONDARY_GRADIENT = make_gradient_cmap(
    "crossref-gradient-secondary",
    CR_SECONDARY_GREEN,
    CR_SECONDARY_YELLOW,
    CR_SECONDARY_RED,
)
CR_MONO_GRADIENT = make_gradient_cmap(
    "crossref-gradient-mono", CR_MONO_1, CR_MONO_3, CR_MONO_5
)


def __main__():
    show_cmaps(
        [
            "crossref-gradient-primary",
            "crossref-gradient-secondary",
            "crossref-gradient-mono",
            "crossref-primary",
            "crossref-secondary",
            "crossref-mono",
        ]
    )
