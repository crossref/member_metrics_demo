# syntax=docker/dockerfile:1
FROM python:3.10-slim
RUN apt-get update; apt-get install -y curl
# unzip gcc python3-dev
WORKDIR /code
# set virtual env
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
# RUN od -An -N1 -i /dev/random > random_num

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
# RUN ls
COPY . .
EXPOSE 5000
ARG CACHE_DATE=2023-07-03

# RUN apt-get install curl &&  curl -SL "https://api.labs.crossref.org/members/?mailto=labs@crossref.org&rows=all" -o members.json
# RUN python3 prepare-data.py --verbose
# RUN rm members.json
RUN echo $GIT_REVISION > REVISION

# NB turn off file watcher for streamlit because iwatch doesn't like working with so many
# files in the data directory in the container
CMD ["streamlit", "run","app.py"]
